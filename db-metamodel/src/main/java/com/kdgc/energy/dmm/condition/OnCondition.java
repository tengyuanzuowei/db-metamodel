package com.kdgc.energy.dmm.condition;

import com.kdgc.energy.dmm.Environment;
import com.kdgc.energy.dmm.lambda.SerializedFunction;
import com.kdgc.energy.dmm.query.Select;
import org.apache.metamodel.query.FilterItem;

/**
 * on条件
 *
 * @author xu.wenchang
 * @version 1.0 2022/04/25
 */
public class OnCondition extends Condition {
    public <T1, R1, T2, R2> OnCondition(SerializedFunction<T1, R1> sf1, SerializedFunction<T2, R2> sf2) {
        super(new FilterItem(Environment.getColumnName(sf1)
                                        .concat(" = ")
                                        .concat(Environment.getColumnName(sf2))));
    }
    
    public OnCondition(Select s1, Select s2) {
        super(new FilterItem(s1.getDelegate()
                               .toSql()
                               .concat(" = ")
                               .concat(s2.getDelegate()
                                         .toSql())));
    }
    
}
