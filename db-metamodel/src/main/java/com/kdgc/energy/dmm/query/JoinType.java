package com.kdgc.energy.dmm.query;

/**
 * 连接类型
 *
 * @author xu.wenchang
 * @version 1.0 2022/04/16
 */
public enum JoinType {
    LEFT, INNER, RIGHT
}
