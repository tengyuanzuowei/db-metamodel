package com.kdgc.energy.dmm.entity;

import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Field;

/**
 * 字段信息
 * <p>
 * 包含：类字段、表字段名
 *
 * @author xu.wenchang
 * @version 1.0 2022/04/14
 */
@Getter
@Setter
public class FieldInfo {
    private Field field;
    private String columnName;
    private boolean isPk;
}
