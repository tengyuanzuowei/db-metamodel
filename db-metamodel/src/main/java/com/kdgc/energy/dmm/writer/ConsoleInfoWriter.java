package com.kdgc.energy.dmm.writer;

import com.kdgc.energy.dmm.InfoWriter;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/21
 */
public class ConsoleInfoWriter implements InfoWriter {
    @Override
    public void write(String info) {
        System.out.println(info);
    }
    
    @Override
    public void write(Object obj) {
        System.out.println(obj);
    }
}
