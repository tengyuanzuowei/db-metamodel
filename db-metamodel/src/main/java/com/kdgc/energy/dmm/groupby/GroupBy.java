package com.kdgc.energy.dmm.groupby;

import com.kdgc.energy.dmm.Environment;
import com.kdgc.energy.dmm.lambda.SerializedFunction;
import com.kdgc.energy.dmm.query.Select;
import lombok.Getter;
import org.apache.metamodel.query.GroupByItem;
import org.apache.metamodel.query.SelectItem;

import java.util.Arrays;
import java.util.List;

/**
 * group by
 *
 * @author xu.wenchang
 * @version 1.0 2022/04/27
 */
public class GroupBy {
    @Getter
    private GroupByItem[] arr;
    
    public GroupBy(Select... selects) {
        this.arr = Arrays.stream(selects)
                         .map(select -> new GroupByItem(select.getDelegate()))
                         .toArray(GroupByItem[]::new);
    }
    
    public <T, R> GroupBy(SerializedFunction<T, R> sf) {
        this.arr = new GroupByItem[]{new GroupByItem(new SelectItem(Environment.get(sf)))};
    }
    
    public GroupBy(List<Select> list) {
        this.arr = list.stream()
                       .map(select -> new GroupByItem(select.getDelegate()))
                       .toArray(GroupByItem[]::new);
    }
}
