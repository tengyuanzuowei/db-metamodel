package com.kdgc.energy.dmm.join;

import com.kdgc.energy.dmm.Environment;
import com.kdgc.energy.dmm.lambda.SerializedFunction;
import lombok.Getter;
import org.apache.metamodel.query.FromItem;
import org.apache.metamodel.query.JoinType;
import org.apache.metamodel.query.SelectItem;
import org.apache.metamodel.schema.MutableColumn;

/**
 * join
 *
 * @author xu.wenchang
 * @version 1.0 2022/04/27
 */
public abstract class Join {
    
    @Getter
    private Join0 join0;
    
    @Getter
    private FromItem fromItem;
    
    protected <T1, R1, T2, R2> Join(JoinType joinType, SerializedFunction<T1, R1> leftSF, SerializedFunction<T2, R2> rightSF) {
        MutableColumn leftCol = Environment.get(leftSF);
        MutableColumn rightCol = Environment.get(rightSF);
        this.join0 = new Join0(leftCol, rightCol, joinType);
    }
    
    protected <T1, R1, T2, R2> Join(JoinType joinType, SerializedFunction<T1, R1> leftSF1, SerializedFunction<T2, R2> rightSF1, SerializedFunction<T1, R1> leftSF2, SerializedFunction<T2, R2> rightSF2) {
        SelectItem[] leftOn = new SelectItem[]{new SelectItem(Environment.get(leftSF1)), new SelectItem(Environment.get(leftSF2))};
        SelectItem[] rightOn = new SelectItem[]{new SelectItem(Environment.get(rightSF1)), new SelectItem(Environment.get(rightSF2))};
        this.fromItem = new FromItem(joinType, new FromItem(Environment.get(leftSF1)
                                                                       .getTable()), new FromItem(Environment.get(rightSF1)
                                                                                                             .getTable()), leftOn, rightOn);
    }
}
