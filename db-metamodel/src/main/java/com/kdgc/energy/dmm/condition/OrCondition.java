package com.kdgc.energy.dmm.condition;

import org.apache.metamodel.query.FilterItem;
import org.apache.metamodel.query.LogicalOperator;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * or条件
 *
 * @author xu.wenchang
 * @version 1.0 2022/04/25
 */
public class OrCondition extends Condition {
    public OrCondition(List<Condition> items) {
        super(new FilterItem(LogicalOperator.OR, items.stream()
                                                      .map(Condition::getDelegate)
                                                      .collect(Collectors.toList())));
    }
    
    
    public OrCondition(Condition... items) {
        super(new FilterItem(LogicalOperator.OR, Arrays.stream(items)
                                                       .map(Condition::getDelegate)
                                                       .collect(Collectors.toList())));
    }
}
