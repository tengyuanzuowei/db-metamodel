package com.kdgc.energy.dmm.result;

import cn.hutool.core.util.StrUtil;
import com.kdgc.energy.dmm.table.Table;
import org.apache.metamodel.data.DataSet;
import org.apache.metamodel.data.Row;
import org.apache.metamodel.query.SelectItem;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/24
 */
public class ListResult extends AbstractResult {
    
    /**
     * 注意：不支持内部类
     */
    @SuppressWarnings("unchecked")
    public <T> List<T> list(DataSet dataSet, Class<T> targetClass) {
        List<Object> list = new ArrayList<>();
        Field[] fields = targetClass.getDeclaredFields();
        
        Map<String, Field> fieldMap = Arrays.stream(fields)
                                            .filter(f -> !Modifier.isStatic(f.getModifiers()))
                                            .collect(Collectors.toMap(Field::getName, field -> field));
        
        while (dataSet.next()) {
            Row row = dataSet.getRow();
            try {
                Object inst = fillObject(targetClass, row, fieldMap);
                list.add(inst);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        
        return Collections.unmodifiableList((List<T>) list);
    }
    
    @SuppressWarnings("unchecked")
    public <T> List<T> list(DataSet dataSet) {
        List<Table> tables = getTableInfo(dataSet);
        
        if (null == tables) {
            List<T> list = new ArrayList<>();
            while (dataSet.next()) {
                Row row = dataSet.getRow();
                try {
                    Map<String, Object> map = new HashMap<>();
                    
                    for (SelectItem si : row.getSelectItems()) {
                        String key;
                        if (null == si.getAlias() && null == si.getColumn()) {
                            SelectItem subQuerySi = si.getSubQuerySelectItem();
                            key = StrUtil.isBlank(subQuerySi.getAlias()) ? subQuerySi.getColumn()
                                                                                     .getName() : subQuerySi.getAlias();
                        } else {
                            key = StrUtil.isBlank(si.getAlias()) ? si.getColumn()
                                                                     .getName() : si.getAlias();
                        }
                        
                        map.put(key, row.getValue(si));
                    }
                    
                    list.add((T) map);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
            return Collections.unmodifiableList(list);
        }
        
        boolean multiTable = tables.size() > 1;
        
        if (multiTable) {
            List<T> list = new ArrayList<>();
            while (dataSet.next()) {
                Row row = dataSet.getRow();
                try {
                    Map<String, Object> map = new HashMap<>();
                    
                    for (SelectItem si : row.getSelectItems()) {
                        String key = StrUtil.isBlank(si.getAlias()) ? si.getColumn()
                                                                        .getName() : si.getAlias();
                        map.put(key, row.getValue(si));
                    }
                    
                    list.add((T) map);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
            return Collections.unmodifiableList(list);
        } else {
            List<T> list = new ArrayList<>();
            Table table = tables.get(0);
            Map<String, Field> fieldMap = buildFieldMap(table);
            List<SelectItem> selectItems = dataSet.getSelectItems();
            int columnNum = selectItems.size();
            
            while (dataSet.next()) {
                Row row = dataSet.getRow();
                try {
                    if (columnNum > 1) {
                        Class<?> entityClass = table.getEntityInfo()
                                                    .getEntityClass();
                        Object inst = fillObject(entityClass, row, fieldMap);
                        list.add((T) inst);
                    } else {
                        list.add((T) row.getValue(0));
                    }
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
            return Collections.unmodifiableList(list);
        }
    }
}
