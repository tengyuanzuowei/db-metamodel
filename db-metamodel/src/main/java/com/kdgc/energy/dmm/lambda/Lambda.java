package com.kdgc.energy.dmm.lambda;

import lombok.Getter;
import lombok.Setter;

/**
 * lambda
 *
 * @author xu.wenchang
 * @version 1.0 2022/04/14
 */
@Getter
@Setter
public class Lambda {
    private String methodName;
    private String className;
    private Class<?> clazz;
    
    @Override
    public String toString() {
        return "method:" + methodName + ", className:" + className + ", clazz:" + clazz;
    }
}
