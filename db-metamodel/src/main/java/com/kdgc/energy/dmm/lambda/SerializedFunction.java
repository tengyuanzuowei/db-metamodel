package com.kdgc.energy.dmm.lambda;

import java.io.Serializable;
import java.util.function.Function;

/**
 * 序列化的Function
 * <p>
 * 简单定制Function，使其可序列化，方便后续从字节流读取lambda对象，这是关键
 *
 * @author xu.wenchang
 * @version 1.0 2022/04/14
 */
@FunctionalInterface
public interface SerializedFunction<T, R> extends Function<T, R>, Serializable {
}
