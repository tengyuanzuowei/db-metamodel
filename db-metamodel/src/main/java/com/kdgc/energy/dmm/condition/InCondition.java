package com.kdgc.energy.dmm.condition;

import com.kdgc.energy.dmm.Environment;
import com.kdgc.energy.dmm.lambda.SerializedFunction;
import com.kdgc.energy.dmm.query.Query;
import com.kdgc.energy.dmm.query.Select;
import org.apache.metamodel.query.FilterItem;
import org.apache.metamodel.query.OperatorType;

/**
 * IN条件
 *
 * @author xu.wenchang
 * @version 1.0 2022/04/25
 */
public class InCondition extends Condition {
    
    public <T, R> InCondition(SerializedFunction<T, R> sf, Query subQuery) {
        super(new FilterItem(Environment.getColumnName(sf) + " IN (" + subQuery.toSQL() + ")"));
    }
    
    public <T, R> InCondition(SerializedFunction<T, R> sf, Object operand) {
        this(new Select(sf), operand);
    }
    
    public InCondition(Select item, Object operand) {
        super(OperatorType.IN, item, operand);
    }
}
