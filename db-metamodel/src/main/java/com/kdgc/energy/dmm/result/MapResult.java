package com.kdgc.energy.dmm.result;

import cn.hutool.core.util.StrUtil;
import org.apache.metamodel.data.DataSet;
import org.apache.metamodel.data.Row;
import org.apache.metamodel.query.SelectItem;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/24
 */
public class MapResult extends AbstractResult {
    public Map<String, Object> map(DataSet dataSet) {
        Map<String, Object> map = new HashMap<>();
        
        if (dataSet.next()) {
            List<SelectItem> selectItems = dataSet.getSelectItems();
            Row row = dataSet.getRow();
            
            for (SelectItem si : selectItems) {
                map.put(StrUtil.isNotBlank(si.getAlias()) ? si.getAlias() : si.getColumn()
                                                                              .getName(), row.getValue(si));
            }
        }
        
        return Collections.unmodifiableMap(map);
    }
}
