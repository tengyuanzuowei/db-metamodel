package com.kdgc.energy.dmm.condition;

import com.kdgc.energy.dmm.lambda.SerializedFunction;
import com.kdgc.energy.dmm.query.Select;
import org.apache.metamodel.query.OperatorType;

/**
 * 不等于条件
 *
 * @author xu.wenchang
 * @version 1.0 2022/04/25
 */
public class NEQCondition extends Condition {
    public <T, R> NEQCondition(SerializedFunction<T, R> sf, Object operand) {
        this(new Select(sf), operand);
    }
    
    public NEQCondition(Select item, Object operand) {
        super(OperatorType.DIFFERENT_FROM, item, operand);
    }
}
