package com.kdgc.energy.dmm.condition;

import com.kdgc.energy.dmm.lambda.SerializedFunction;
import com.kdgc.energy.dmm.query.Select;
import org.apache.metamodel.query.OperatorType;

/**
 * 小于条件
 *
 * @author xu.wenchang
 * @version 1.0 2022/04/25
 */
public class LTCondition extends Condition {
    public <T, R> LTCondition(SerializedFunction<T, R> sf, Object operand) {
        this(new Select(sf), operand);
    }
    
    public LTCondition(Select item, Object operand) {
        super(OperatorType.LESS_THAN, item, operand);
    }
}
