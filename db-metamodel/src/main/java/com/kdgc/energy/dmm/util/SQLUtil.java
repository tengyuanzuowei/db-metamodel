package com.kdgc.energy.dmm.util;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/24
 */
public final class SQLUtil {
    
    public static String buildBetweenSQL(Object left, Object right) {
        return " BETWEEN '" + left.toString() + "' AND '" + right.toString() + "'";
    }
}
