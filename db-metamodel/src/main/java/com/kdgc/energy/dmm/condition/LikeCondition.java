package com.kdgc.energy.dmm.condition;

import com.kdgc.energy.dmm.lambda.SerializedFunction;
import com.kdgc.energy.dmm.query.Select;
import org.apache.metamodel.query.OperatorType;

/**
 * like条件
 *
 * @author xu.wenchang
 * @version 1.0 2022/04/25
 */
public class LikeCondition extends Condition {
    public <T, R> LikeCondition(SerializedFunction<T, R> sf, Object operand) {
        this(new Select(sf), operand);
    }
    
    public LikeCondition(Select item, Object operand) {
        super(OperatorType.LIKE, item, operand);
    }
}
