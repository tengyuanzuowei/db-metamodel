package com.kdgc.energy.dmm.condition;

import com.kdgc.energy.dmm.Environment;
import com.kdgc.energy.dmm.lambda.SerializedFunction;
import com.kdgc.energy.dmm.query.Select;
import com.kdgc.energy.dmm.util.SQLUtil;
import org.apache.metamodel.query.FilterItem;

/**
 * between and条件
 *
 * @author xu.wenchang
 * @version 1.0 2022/04/25
 */
public class BetweenCondition extends Condition {
    public <T, R> BetweenCondition(SerializedFunction<T, R> sf, Object left, Object right) {
        super(new FilterItem(Environment.getColumnName(sf)
                                        .concat(SQLUtil.buildBetweenSQL(left, right))));
    }
    
    public BetweenCondition(Select item, Object left, Object right) {
        super(new FilterItem(item.getDelegate()
                                 .toSql()
                                 .concat(SQLUtil.buildBetweenSQL(left, right))));
    }
}
