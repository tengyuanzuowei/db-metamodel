package com.kdgc.energy.dmm.query;

import lombok.Getter;

/**
 * 分页查询
 *
 * @author xu.wenchang
 * @version 1.0 2022/04/16
 */
@Getter
public class PageQuery extends Query {
    private int page;
    private int size;
    
    public PageQuery(int page, int size) {
        assert page < 1 : "page必须为正数.";
        assert size < 1 : "size必须为正数.";
        
        this.page = page;
        this.size = size;
        int first = (this.page - 1) * size;
        this.query.setFirstRow(first + 1);
        this.query.setMaxRows(size);
    }
}
