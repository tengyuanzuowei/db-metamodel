package com.kdgc.energy.dmm.condition;

import com.kdgc.energy.dmm.lambda.SerializedFunction;
import com.kdgc.energy.dmm.query.Select;
import org.apache.metamodel.query.OperatorType;

/**
 * 不包含条件
 *
 * @author xu.wenchang
 * @version 1.0 2022/04/25
 */
public class NotInCondition extends Condition {
    public <T, R> NotInCondition(SerializedFunction<T, R> sf, Object operand) {
        this(new Select(sf), operand);
    }
    
    public NotInCondition(Select item, Object operand) {
        super(OperatorType.NOT_IN, item, operand);
    }
}
