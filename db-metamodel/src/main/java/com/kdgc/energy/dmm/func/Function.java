package com.kdgc.energy.dmm.func;

/**
 * 函数
 *
 * @author xu.wenchang
 * @version 1.0 2022/04/16
 */
public enum Function {
    /**
     * 计数
     */
    COUNT,
    /**
     * 平均数
     */
    AVG,
    /**
     * 求和
     */
    SUM,
    /**
     * 最大值
     */
    MAX,
    /**
     * 最小值
     */
    MIN,
    /**
     * 随机数
     */
    RANDOM,
    /**
     * 第一个
     */
    FIRST,
    /**
     * 最后一个
     */
    LAST,
    /**
     * 转string
     */
    TO_STRING,
    /**
     * 转数字
     */
    TO_NUMBER,
    /**
     * 转日期
     */
    TO_DATE,
    /**
     * 转布尔
     */
    TO_BOOLEAN,
    /**
     * 字符串截取
     */
    SUBSTRING,
    /**
     * java字符串截取
     */
    JAVA_SUBSTRING,
    /**
     * map
     */
    MAP_VALUE
}
