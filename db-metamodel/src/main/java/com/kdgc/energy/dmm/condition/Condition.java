package com.kdgc.energy.dmm.condition;

import com.kdgc.energy.dmm.query.Select;
import lombok.Getter;
import org.apache.metamodel.query.FilterItem;
import org.apache.metamodel.query.OperatorType;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/16
 */
public class Condition {
    @Getter
    private FilterItem delegate;
    
    protected Condition(FilterItem delegate) {
        this.delegate = delegate;
    }
    
    protected Condition(OperatorType ot, Select item, Object operand) {
        this.delegate = new FilterItem(item.getDelegate(), ot, operand);
    }
    
    public String toSQL() {
        return this.delegate.toSql();
    }
}
