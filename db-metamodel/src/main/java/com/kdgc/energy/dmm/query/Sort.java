package com.kdgc.energy.dmm.query;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/16
 */
public enum Sort {
    ASC, DESC
}
