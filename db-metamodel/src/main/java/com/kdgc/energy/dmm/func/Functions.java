package com.kdgc.energy.dmm.func;

import org.apache.metamodel.query.FunctionType;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/16
 */
public class Functions {
    public static FunctionType toMetaModelFunctionType(Function function) {
        switch (function) {
            case COUNT:
                return FunctionType.COUNT;
            case AVG:
                return FunctionType.AVG;
            case SUM:
                return FunctionType.SUM;
            case MAX:
                return FunctionType.MAX;
            case MIN:
                return FunctionType.MIN;
            case RANDOM:
                return FunctionType.RANDOM;
            case FIRST:
                return FunctionType.FIRST;
            case LAST:
                return FunctionType.LAST;
            case TO_STRING:
                return FunctionType.TO_STRING;
            case TO_NUMBER:
                return FunctionType.TO_NUMBER;
            case TO_DATE:
                return FunctionType.TO_DATE;
            case TO_BOOLEAN:
                return FunctionType.TO_BOOLEAN;
            case SUBSTRING:
                return FunctionType.SUBSTRING;
            case JAVA_SUBSTRING:
                return FunctionType.JAVA_SUBSTRING;
            case MAP_VALUE:
                return FunctionType.MAP_VALUE;
            default:
                throw new UnsupportedOperationException("未定义的函数");
        }
    }
}
