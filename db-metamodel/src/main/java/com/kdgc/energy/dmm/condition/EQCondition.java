package com.kdgc.energy.dmm.condition;

import com.kdgc.energy.dmm.lambda.SerializedFunction;
import com.kdgc.energy.dmm.query.Select;
import org.apache.metamodel.query.OperatorType;

/**
 * 等于条件
 *
 * @author xu.wenchang
 * @version 1.0 2022/04/25
 */
public class EQCondition extends Condition {
    public <T, R> EQCondition(SerializedFunction<T, R> sf, Object operand) {
        this(new Select(sf), operand);
    }
    
    public EQCondition(Select item, Object operand) {
        super(OperatorType.EQUALS_TO, item, operand);
    }
}
