package com.kdgc.energy.dmm.result;

import cn.hutool.core.util.StrUtil;
import com.kdgc.energy.dmm.table.Table;
import org.apache.metamodel.data.DataSet;
import org.apache.metamodel.data.Row;
import org.apache.metamodel.query.SelectItem;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 单一结果
 *
 * @author xu.wenchang
 * @version 1.0 2022/04/24
 */
public class SingleResult extends AbstractResult {
    @SuppressWarnings("unchecked")
    public <T> T one(DataSet dataSet, Class<?> targetClass) {
        Field[] fields = targetClass.getDeclaredFields();
        
        Map<String, Field> fieldMap = Arrays.stream(fields)
                                            .filter(f -> !Modifier.isStatic(f.getModifiers()))
                                            .collect(Collectors.toMap(Field::getName, field -> field));
        
        if (dataSet.next()) {
            Row row = dataSet.getRow();
            try {
                Object inst = fillObject(targetClass, row, fieldMap);
                return (T) inst;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        
        return null;
    }
    
    @SuppressWarnings("unchecked")
    public <T> T one(DataSet dataSet) {
        List<Table> tables = this.getTableInfo(dataSet);
        
        if (null == tables) {
            if (dataSet.next()) {
                Row row = dataSet.getRow();
                try {
                    Map<String, Object> map = new HashMap<>();
                    
                    for (SelectItem si : row.getSelectItems()) {
                        String key = StrUtil.isBlank(si.getAlias()) ? si.getColumn()
                                                                        .getName() : si.getAlias();
                        map.put(key, row.getValue(si));
                    }
                    return (T) map;
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
            return null;
        }
        
        if (tables.size() > 1) {
            if (dataSet.next()) {
                Row row = dataSet.getRow();
                try {
                    Map<String, Object> map = new HashMap<>();
                    
                    for (SelectItem si : row.getSelectItems()) {
                        String key = StrUtil.isBlank(si.getAlias()) ? si.getColumn()
                                                                        .getName() : si.getAlias();
                        map.put(key, row.getValue(si));
                    }
                    return (T) map;
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
            return null;
        } else {
            Table table = tables.get(0);
            Class<?> entityClass = table.getEntityInfo()
                                        .getEntityClass();
            Map<String, Field> fieldMap = this.buildFieldMap(table);
            
            if (dataSet.next()) {
                Row row = dataSet.getRow();
                try {
                    Object inst = fillObject(entityClass, row, fieldMap);
                    return (T) inst;
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
            return null;
        }
    }
}
