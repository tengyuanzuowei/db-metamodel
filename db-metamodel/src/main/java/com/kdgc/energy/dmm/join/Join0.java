package com.kdgc.energy.dmm.join;

import lombok.Getter;
import org.apache.metamodel.query.JoinType;
import org.apache.metamodel.schema.MutableColumn;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/27
 */
@Getter
public class Join0 {
    MutableColumn leftCol;
    MutableColumn rightCol;
    JoinType joinType;
    
    Join0(MutableColumn leftCol, MutableColumn rightCol, JoinType joinType) {
        this.leftCol = leftCol;
        this.rightCol = rightCol;
        this.joinType = joinType;
    }
}
