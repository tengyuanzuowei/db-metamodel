package com.kdgc.energy.dmm.util;

import org.apache.metamodel.schema.MutableColumn;
import org.apache.metamodel.schema.MutableTable;

/**
 * 名称类工具
 *
 * @author xu.wenchang
 * @version 1.0 2022/04/02
 */
public class NameUtil {
    
    /**
     * 将驼峰式名称转成下划线式
     * <p>
     * eg.  axxBxxCxx -> axx_bxx_cxx
     */
    public static String camel2underline(String src) {
        int length = src.length();
        int index = 0;
        StringBuilder builder = new StringBuilder();
        
        while (index < length) {
            char ch = src.charAt(index);
            if (Character.isUpperCase(ch)) {
                if (index != 0) {
                    builder.append("_");
                }
                
                builder.append(Character.toLowerCase(ch));
            } else {
                builder.append(ch);
            }
            
            index++;
        }
        
        return builder.toString();
    }
    
    /**
     * 方法名转字段名
     */
    public static String methodName2FieldName(String methodName) {
        String fieldName = methodName.replaceAll("(get)|(set)", "");
        return Character.toLowerCase(fieldName.charAt(0)) + fieldName.substring(1);
    }
    
    public static String getTableName(MutableTable table) {
        return table.getQualifiedLabel();
    }
    
    public static String getColumnName(MutableColumn column) {
        return column.getQualifiedLabel();
    }
    
    public static void main(String[] args) {
        System.out.println(camel2underline("AbcDef12D"));
        System.out.println(camel2underline("accountItemCode"));
        System.out.println(camel2underline("account"));
        System.out.println(camel2underline("A"));
        System.out.println(camel2underline("getAccountName"));
        System.out.println(camel2underline("setId"));
    }
}
