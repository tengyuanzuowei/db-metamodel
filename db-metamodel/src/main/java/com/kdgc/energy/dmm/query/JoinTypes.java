package com.kdgc.energy.dmm.query;

import org.apache.metamodel.query.JoinType;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/16
 */
public class JoinTypes {
    public static JoinType toMetaModelJoinType(com.kdgc.energy.dmm.query.JoinType joinType) {
        switch (joinType) {
            case LEFT:
                return JoinType.LEFT;
            case INNER:
                return JoinType.INNER;
            case RIGHT:
                return JoinType.RIGHT;
            default:
                throw new UnsupportedOperationException("未定义连接");
        }
    }
}

