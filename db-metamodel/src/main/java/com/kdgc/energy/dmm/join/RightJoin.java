package com.kdgc.energy.dmm.join;

import com.kdgc.energy.dmm.lambda.SerializedFunction;
import org.apache.metamodel.query.JoinType;

/**
 * right join
 *
 * @author xu.wenchang
 * @version 1.0 2022/04/27
 */
public class RightJoin extends Join {
    public <T1, R1, T2, R2> RightJoin(SerializedFunction<T1, R1> leftSF, SerializedFunction<T2, R2> rightSF) {
        super(JoinType.RIGHT, leftSF, rightSF);
    }
    
    public <T1, R1, T2, R2> RightJoin(SerializedFunction<T1, R1> leftSF1, SerializedFunction<T2, R2> rightSF1, SerializedFunction<T1, R1> leftSF2, SerializedFunction<T2, R2> rightSF2) {
        super(JoinType.RIGHT, leftSF1, rightSF1, leftSF2, rightSF2);
    }
}
