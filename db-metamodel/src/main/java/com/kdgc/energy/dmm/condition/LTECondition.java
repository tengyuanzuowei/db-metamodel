package com.kdgc.energy.dmm.condition;

import com.kdgc.energy.dmm.lambda.SerializedFunction;
import com.kdgc.energy.dmm.query.Select;
import org.apache.metamodel.query.OperatorType;

/**
 * 小于等于条件
 *
 * @author xu.wenchang
 * @version 1.0 2022/04/25
 */
public class LTECondition extends Condition {
    public <T, R> LTECondition(SerializedFunction<T, R> sf, Object operand) {
        this(new Select(sf), operand);
    }
    
    public LTECondition(Select item, Object operand) {
        super(OperatorType.LESS_THAN_OR_EQUAL, item, operand);
    }
}
