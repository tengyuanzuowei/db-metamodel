package com.kdgc.energy.dmm.condition;

import org.apache.metamodel.query.FilterItem;
import org.apache.metamodel.query.LogicalOperator;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * and条件
 *
 * @author xu.wenchang
 * @version 1.0 2022/04/25
 */
public class AndCondition extends Condition {
    public AndCondition(List<Condition> items) {
        super(new FilterItem(LogicalOperator.AND, items.stream()
                                                       .map(Condition::getDelegate)
                                                       .collect(Collectors.toList())));
    }
    
    public AndCondition(Condition... items) {
        super(new FilterItem(LogicalOperator.AND, Arrays.stream(items)
                                                        .map(Condition::getDelegate)
                                                        .collect(Collectors.toList())));
    }
}
