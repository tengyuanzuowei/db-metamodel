package com.kdgc.energy.dmm;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/21
 */
public interface InfoWriter {
    
    void write(String info);
    
    void write(Object obj);
}
