package com.kdgc.energy.dmm.entity;

import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Method;

/**
 * 方法信息
 * <p>
 * 包含类方法、表字段名
 *
 * @author xu.wenchang
 * @version 1.0 2022/04/14
 */
@Getter
@Setter
public class MethodInfo {
    private Method method;
    private String columnName;
    private boolean isPk;
}
