package com.kdgc.energy.dmm.join;

import com.kdgc.energy.dmm.lambda.SerializedFunction;
import org.apache.metamodel.query.JoinType;

/**
 * left join
 *
 * @author xu.wenchang
 * @version 1.0 2022/04/27
 */
public class LeftJoin extends Join {
    public <T1, R1, T2, R2> LeftJoin(SerializedFunction<T1, R1> leftSF, SerializedFunction<T2, R2> rightSF) {
        super(JoinType.LEFT, leftSF, rightSF);
    }
    
    public <T1, R1, T2, R2> LeftJoin(SerializedFunction<T1, R1> leftSF1, SerializedFunction<T2, R2> rightSF1, SerializedFunction<T1, R1> leftSF2, SerializedFunction<T2, R2> rightSF2) {
        super(JoinType.LEFT, leftSF1, rightSF1, leftSF2, rightSF2);
    }
}
