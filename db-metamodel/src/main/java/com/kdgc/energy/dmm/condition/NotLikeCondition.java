package com.kdgc.energy.dmm.condition;

import com.kdgc.energy.dmm.lambda.SerializedFunction;
import com.kdgc.energy.dmm.query.Select;
import org.apache.metamodel.query.OperatorType;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/25
 */
public class NotLikeCondition extends Condition {
    public <T, R> NotLikeCondition(SerializedFunction<T, R> sf, Object operand) {
        this(new Select(sf), operand);
    }
    
    public NotLikeCondition(Select item, Object operand) {
        super(OperatorType.NOT_LIKE, item, operand);
    }
}
