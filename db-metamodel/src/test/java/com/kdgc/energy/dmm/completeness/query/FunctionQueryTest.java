package com.kdgc.energy.dmm.completeness.query;

import com.kdgc.energy.dmm.EnvConfig;
import com.kdgc.energy.dmm.completeness.AbstractTest;
import com.kdgc.energy.dmm.completeness.entity.Salary;
import com.kdgc.energy.dmm.query.Query;
import com.kdgc.energy.dmm.time.TimeAnalyser;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/21
 */
public class FunctionQueryTest extends AbstractTest {
    public static void main(String[] args) {
        EnvConfig.setDataSource();
        EnvConfig.onDebug();
    
        max();
        
        TimeAnalyser.INST.close();
    }
    
    private static void max() {
        int max = new Query().from(Salary.class)
                             .max(Salary::getSalary)
                             .execute()
                             .getInt();
        printObj(max);
    }
    
    private static void min() {
        int min = new Query().from(Salary.class)
                             .min(Salary::getSalary)
                             .execute()
                             .getInt();
        printObj(min);
    }
    
    private static void sum() {
        Long sum = new Query().from(Salary.class)
                              .sum(Salary::getSalary, "sum")
                              .execute()
                              .getLong();
        printObj(sum);
    }
    
    private static void avg() {
        float num = new Query().from(Salary.class)
                               .average(Salary::getSalary, "avg")
                               .execute()
                               .getFloat();
        printObj(num);
    }
    
    private static void count() {
        int num = new Query().from(Salary.class)
                             .count(Salary::getEmpNo)
                             .execute()
                             .getInt();
        printObj(num);
    }
    
}
