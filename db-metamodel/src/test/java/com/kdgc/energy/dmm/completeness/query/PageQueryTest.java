package com.kdgc.energy.dmm.completeness.query;

import com.kdgc.energy.dmm.EnvConfig;
import com.kdgc.energy.dmm.completeness.AbstractTest;
import com.kdgc.energy.dmm.completeness.entity.Employee;
import com.kdgc.energy.dmm.condition.LikeCondition;
import com.kdgc.energy.dmm.query.PageQuery;

import java.util.List;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/18
 */
public class PageQueryTest extends AbstractTest {
    public static void main(String[] args) {
        EnvConfig.setDataSource();
        page1();
//        page2();
//        page3();
    }
    
    private static void page1() {
        List<Employee> list = new PageQuery(1, 3).from(Employee.class)
                                                 .select()
                                                 .where(new LikeCondition(Employee::getFirstName, "%jan%"))
                                                 .execute()
                                                 .list();
        
        printList(list);
    }
    
    private static void page2() {
        List<Employee> list = new PageQuery(2, 3).from(Employee.class)
                                                 .select()
                                                 .where(new LikeCondition(Employee::getFirstName, "%jan%"))
                                                 .execute()
                                                 .list();
        
        printList(list);
    }
    
    private static void page3() {
        List<Employee> list = new PageQuery(2, 3).from(Employee.class)
                                                 .select()
                                                 .where(new LikeCondition(Employee::getFirstName, "%jan%"))
                                                 .execute()
                                                 .list();
        
        printList(list);
    }
}
