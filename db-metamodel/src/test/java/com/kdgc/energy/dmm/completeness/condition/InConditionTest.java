package com.kdgc.energy.dmm.completeness.condition;

import com.kdgc.energy.dmm.completeness.AbstractTest;
import com.kdgc.energy.dmm.completeness.entity.Department;
import com.kdgc.energy.dmm.completeness.entity.Employee;
import com.kdgc.energy.dmm.completeness.entity.Salary;
import com.kdgc.energy.dmm.condition.BetweenCondition;
import com.kdgc.energy.dmm.condition.Condition;
import com.kdgc.energy.dmm.condition.InCondition;
import com.kdgc.energy.dmm.func.Function;
import com.kdgc.energy.dmm.query.Query;
import com.kdgc.energy.dmm.query.Select;

import java.util.Arrays;
import java.util.List;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/25
 */
public class InConditionTest extends AbstractTest {
    public static void main(String[] args) {
        withFunctionInList();
    }
    
    private static void subQuery() {
        Condition condition = new InCondition(Salary::getEmpNo, new Query().from(Employee.class)
                                                                           .select(Employee::getEmpNo)
                                                                           .where(new BetweenCondition(Employee::getBirthDate, "1959-12-03", "1959-12-05")));
        printObj(condition.toSQL());
    }
    
    private static void inList() {
        List<String> list = Arrays.asList("Production", "Human Resources");
        Condition condition = new InCondition(Department::getDeptNo, list);
        printObj(condition.toSQL());
    }
    
    private static void inArray() {
        String[] arr = {"Production", "Human Resources"};
        Condition condition = new InCondition(Department::getDeptNo, arr);
        printObj(condition.toSQL());
    }
    
    private static void withFunctionInList() {
        List<String> list = Arrays.asList("Prod", "Huma");
        Select select = new Select(Function.SUBSTRING, new Object[]{1, 4}, Department::getDeptName);
        Condition condition = new InCondition(select, list);
        printObj(condition.toSQL());
    }
}
