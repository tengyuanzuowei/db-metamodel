package com.kdgc.energy.dmm;

import cn.hutool.db.ds.simple.SimpleDataSource;
import com.kdgc.energy.dmm.writer.ConsoleInfoWriter;

import javax.sql.DataSource;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/18
 */
public class EnvConfig {
    public static void setDataSource() {
        DataSource ds = new SimpleDataSource("jdbc:mysql://localhost:3306/employees", "root", "123456", "com.mysql.cj" + ".jdbc.Driver");
        Environment.setDataSource(ds);
    }
    
    public static void onDebug() {
        Environment.showSQL();
        Environment.openExecuteTimeStatistics();
        Environment.setInfoWriter(new ConsoleInfoWriter());
    }
}
