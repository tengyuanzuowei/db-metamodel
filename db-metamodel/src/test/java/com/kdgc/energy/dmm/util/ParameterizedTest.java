package com.kdgc.energy.dmm.util;

import cn.hutool.core.util.ClassUtil;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.List;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/17
 */
public class ParameterizedTest<T> {
    
    public static void main(String[] args) throws Exception {
        List<Integer> list = new ParameterizedTest<Integer>().test();
    }
    
    private List<T> test() throws Exception {
        Type type = this.getClass().getGenericSuperclass();
        Class<?> clazz = ClassUtil.getTypeArgument(ParameterizedTest.class);
        System.out.println(clazz);
        if (type instanceof ParameterizedType) {
            ParameterizedType pt = (ParameterizedType) type;
            System.out.println(pt.getActualTypeArguments()[0]);
        }
        
        return null;
    }
}
