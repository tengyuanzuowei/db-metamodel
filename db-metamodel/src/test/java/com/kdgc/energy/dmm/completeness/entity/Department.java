package com.kdgc.energy.dmm.completeness.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/17
 */
@Entity(name = "departments")
@Getter
@Setter
@ToString
public class Department implements Serializable {
    private static final long serialVersionUID = -9208600620675404010L;
    @Id
    @Column
    private String deptNo;
    
    @Column
    private String deptName;
}
