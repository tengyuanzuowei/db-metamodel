package com.kdgc.energy.dmm.completeness.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Date;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/17
 */
@Entity(name = "salaries")
@Getter
@Setter
@ToString
public class Salary implements Serializable {
    private static final long serialVersionUID = 7334869635508885475L;
    @Column
    private Long empNo;
    
    @Column
    private Integer salary;
    
    @Column
    private Date fromDate;
    
    @Column
    private Date toDate;
}
