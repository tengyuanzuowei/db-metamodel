package com.kdgc.energy.dmm.completeness.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Date;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/22
 */
@Getter
@Setter
@ToString
@Entity(name = "dept_manager")
public class DeptManager implements Serializable {
    private static final long serialVersionUID = -340996573794600464L;
    
    @Column
    private Long empNo;
    
    @Column
    private String deptNo;
    
    @Column
    private Date fromDate;
    
    @Column
    private Date toDate;
}
