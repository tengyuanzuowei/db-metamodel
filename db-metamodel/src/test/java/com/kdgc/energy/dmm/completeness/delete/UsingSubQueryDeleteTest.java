package com.kdgc.energy.dmm.completeness.delete;

import com.kdgc.energy.dmm.completeness.AbstractTest;
import com.kdgc.energy.dmm.completeness.entity.Employee;
import com.kdgc.energy.dmm.completeness.entity.Salary;
import com.kdgc.energy.dmm.condition.BetweenCondition;
import com.kdgc.energy.dmm.condition.Condition;
import com.kdgc.energy.dmm.condition.InCondition;
import com.kdgc.energy.dmm.delete.Delete;
import com.kdgc.energy.dmm.query.Query;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/25
 */
public class UsingSubQueryDeleteTest extends AbstractTest {
    
    public static void main(String[] args) {
        onTest();
        usingBetweenQuery();
        close();
    }
    
    private static void usingBetweenQuery() {
        Condition condition = new InCondition(Salary::getEmpNo, new Query().from(Employee.class)
                                                                           .select(Employee::getEmpNo)
                                                                           .where(new BetweenCondition(Employee::getBirthDate, "1959-12-03", "1959-12-05")));
        int num = new Delete(Salary.class).where(condition)
                                          .execute();
        printObj(num);
    }
}
