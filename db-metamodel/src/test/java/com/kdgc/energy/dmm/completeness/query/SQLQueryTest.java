package com.kdgc.energy.dmm.completeness.query;

import com.kdgc.energy.dmm.EnvConfig;
import com.kdgc.energy.dmm.completeness.AbstractTest;
import com.kdgc.energy.dmm.completeness.entity.Employee;
import com.kdgc.energy.dmm.completeness.entity.Salary;
import com.kdgc.energy.dmm.query.SQLQuery;
import com.kdgc.energy.dmm.time.TimeAnalyser;

import java.util.List;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/23
 */
public class SQLQueryTest extends AbstractTest {
    public static void main(String[] args) {
        EnvConfig.setDataSource();
        EnvConfig.onDebug();
        
        List<Employee> list = new SQLQuery(Employee.class, Salary.class).sql(sql())
                                                                        .execute()
                                                                        .list();
        
        printList(list);
        TimeAnalyser.INST.close();
    }
    
    private static String sql() {
        return "SELECT employees.emp_no, employees.birth_date, employees.first_name, employees.last_name, employees" +
                ".gender, employees.hire_date FROM employees WHERE employees.birth_date BETWEEN '1953-09-02' AND '1953-09-06'";
    }
}
