package com.kdgc.energy.dmm.completeness.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Date;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/18
 */
@Entity(name = "dept_emp")
@Getter
@Setter
public class DeptEmp implements Serializable {
    private static final long serialVersionUID = 2589693654527491363L;
    
    @Column
    private Long empNo;
    
    @Column
    private String deptNo;
    
    @Column
    private Date fromDate;
    
    @Column
    private Date toDate;
}
