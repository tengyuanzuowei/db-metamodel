package com.kdgc.energy.dmm.completeness.groupby;

import com.kdgc.energy.dmm.EnvConfig;
import com.kdgc.energy.dmm.completeness.AbstractTest;
import com.kdgc.energy.dmm.completeness.entity.Employee;
import com.kdgc.energy.dmm.completeness.query.LastNameNumDTO;
import com.kdgc.energy.dmm.condition.GTCondition;
import com.kdgc.energy.dmm.func.Function;
import com.kdgc.energy.dmm.query.Query;
import com.kdgc.energy.dmm.query.Select;
import com.kdgc.energy.dmm.time.TimeAnalyser;

import java.util.List;
import java.util.Map;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/18
 */
public class GroupByQueryTest extends AbstractTest {
    public static void main(String[] args) throws Exception {
        EnvConfig.setDataSource();
        EnvConfig.onDebug();
        groupAndHavingMap2DTO();
        TimeAnalyser.INST.close();
    }
    
    // SELECT employees.last_name, COUNT(*) AS num FROM employees GROUP BY employees.last_name HAVING COUNT(employees.first_name) > 200 ORDER BY employees.last_name ASC
    private static void groupAndHaving() {
        List<Map<String, Object>> list = new Query().from(Employee.class)
                                                    .select(Employee::getLastName)
                                                    .count("num")
                                                    .groupBy(Employee::getLastName)
                                                    .having(new GTCondition(new Select(Function.COUNT, Employee::getFirstName), 200))
                                                    .orderBy(Employee::getLastName)
                                                    .execute()
                                                    .list();
        
        printList(list);
    }
    
    private static void groupAndHavingMap2DTO() {
        List<LastNameNumDTO> list = new Query().from(Employee.class)
                                               .select(Employee::getLastName)
                                               .count("num")
                                               .groupBy(Employee::getLastName)
                                               .having(new GTCondition(new Select(Function.COUNT, Employee::getFirstName), 200))
                                               .orderBy(Employee::getLastName)
                                               .execute()
                                               .list(LastNameNumDTO.class);
        
        printList(list);
    }
}
