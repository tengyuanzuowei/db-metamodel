package com.kdgc.energy.dmm.completeness.query;

import com.kdgc.energy.dmm.EnvConfig;
import com.kdgc.energy.dmm.Environment;
import com.kdgc.energy.dmm.completeness.AbstractTest;
import com.kdgc.energy.dmm.completeness.entity.Department;
import com.kdgc.energy.dmm.query.Query;
import com.kdgc.energy.dmm.query.Select;
import com.kdgc.energy.dmm.query.Sort;
import com.kdgc.energy.dmm.time.TimeAnalyser;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/18
 */
public class TotalQueryTest extends AbstractTest {
    public static void main(String[] args) throws Exception {
        EnvConfig.setDataSource();
        
        //查询全表所有数据
        queryTable1();
        
        //查询全表部分字段所有数据
//        queryTable2();
        
        //字段设置别名
//        queryTable3();
        
        //字段排序(升序)
//        queryTable4();
        
        //字段排序(降序)
//        queryTable5();
        
        //多个字段排序
//        queryTable6();
        
        //多个字段排序（升降序）
//        queryTable7();
        
        //只取前5条数据
//        queryTable8();
        
        //只取3-6条数据
//        queryTable9();
        
        testStatistics();
    }
    
    private static void testShowSQL() {
        Environment.showSQL();
        queryTable9();
    }
    
    private static void testStatistics() throws Exception {
        EnvConfig.onDebug();
        
        for (int i = 0; i < 10; i++) {
            queryTable9();
            TimeUnit.SECONDS.sleep(30L);
        }
        
        TimeAnalyser.INST.close();
    }
    
    /**
     * 查询全表所有数据
     */
    private static void queryTable1() {
        List<Department> list = new Query().from(Department.class)
                                           .select()
                                           .execute()
                                           .list();
        printList(list);
    }
    
    /**
     * 查询全表部分字段所有数据
     */
    private static void queryTable2() {
        List<Department> list = new Query().from(Department.class)
                                           .select(Department::getDeptName)
                                           .execute()
                                           .list();
        printList(list);
    }
    
    /**
     * 字段设置别名
     */
    private static void queryTable3() {
        Select si = new Select(Department::getDeptName);
        si.setAlias("dept");
        // 全表查询
        List<Department> list = new Query().from(Department.class)
                                           .select(si)
                                           .execute()
                                           .list();
        printList(list);
    }
    
    /**
     * 字段排序(升序)
     */
    private static void queryTable4() {
        // 全表查询
        List<Department> list = new Query().from(Department.class)
                                           .select()
                                           .orderBy(Department::getDeptName)
                                           .execute()
                                           .list();
        printList(list);
    }
    
    /**
     * 字段排序(降序)
     */
    private static void queryTable5() {
        // 全表查询
        List<Department> list = new Query().from(Department.class)
                                           .select()
                                           .orderBy(Department::getDeptName, Sort.DESC)
                                           .execute()
                                           .list();
        printList(list);
    }
    
    /**
     * 多个字段排序
     */
    private static void queryTable6() {
        List<Department> list = new Query().from(Department.class)
                                           .select()
                                           .orderBy(Department::getDeptName)
                                           .orderBy(Department::getDeptNo)
                                           .execute()
                                           .list();
        printList(list);
    }
    
    /**
     * 多个字段排序（升降序）
     */
    private static void queryTable7() {
        List<Department> list = new Query().from(Department.class)
                                           .select()
                                           .orderBy(Department::getDeptName, Sort.ASC)
                                           .orderBy(Department::getDeptNo, Sort.DESC)
                                           .execute()
                                           .list();
        printList(list);
    }
    
    /**
     * 只取前5条数据
     */
    private static void queryTable8() {
        List<Department> list = new Query().from(Department.class)
                                           .select()
                                           .limit(5)
                                           .execute()
                                           .list();
        printList(list);
    }
    
    /**
     * 只取3-6条数据
     */
    private static void queryTable9() {
        List<Department> list = new Query().from(Department.class)
                                           .select()
                                           .limit(2, 4)
                                           .execute()
                                           .list();
        printList(list);
    }
}
