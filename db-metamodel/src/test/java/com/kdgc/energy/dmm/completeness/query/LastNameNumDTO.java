package com.kdgc.energy.dmm.completeness.query;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/21
 */
@Getter
@Setter
@ToString
public class LastNameNumDTO {
    private String lastName;
    private int num;
}
