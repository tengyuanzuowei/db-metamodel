package com.kdgc.energy.dmm.completeness.update;

import com.kdgc.energy.dmm.EnvConfig;
import com.kdgc.energy.dmm.Environment;
import com.kdgc.energy.dmm.completeness.AbstractTest;
import com.kdgc.energy.dmm.completeness.entity.Department;
import com.kdgc.energy.dmm.completeness.entity.DeptManager;
import com.kdgc.energy.dmm.completeness.entity.Employee;
import com.kdgc.energy.dmm.condition.BetweenCondition;
import com.kdgc.energy.dmm.condition.EQCondition;
import com.kdgc.energy.dmm.condition.InCondition;
import com.kdgc.energy.dmm.condition.NEQCondition;
import com.kdgc.energy.dmm.query.Query;
import com.kdgc.energy.dmm.time.TimeAnalyser;
import com.kdgc.energy.dmm.update.Update;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/19
 */
public class UpdateTest extends AbstractTest {
    
    public static void main(String[] args) throws Exception {
        EnvConfig.setDataSource();
        EnvConfig.onDebug();
        updateUsingSubQuery();
        TimeAnalyser.INST.close();
    }
    
    private static void testShowSQL() {
        Environment.showSQL();
        updateColumn();
    }
    
    private static void testStatistics() throws Exception {
        EnvConfig.onDebug();
        
        for (int i = 0; i < 10; i++) {
            updateColumn();
            TimeUnit.SECONDS.sleep(30L);
        }
        
        TimeAnalyser.INST.close();
    }
    
    private static void updateUsingBetween() {
        int num = new Update(Employee.class).set(Employee::getBirthDate, "1952-09-02")
                                            .where(new BetweenCondition(Employee::getBirthDate, "1953-09-02", "1953-09-04"))
                                            .execute();
        printObj(num);
    }
    
    private static void updateMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("deptNo", "d100");
        map.put("deptName", "test_department3");
        int affectedNums = new Update(Department.class).set(map)
                                                       .execute();
        printObj(affectedNums);
    }
    
    private static void updateEntity() {
        Department department = new Department();
        department.setDeptNo("d100");
        department.setDeptName("test_department2");
        int affectedNums = new Update(Department.class).set(department)
                                                       .execute();
        printObj(affectedNums);
    }
    
    private static void updateColumn() {
        int affectedNums = new Update(Department.class).set(Department::getDeptName, "test_department")
                                                       .where(new EQCondition(Department::getDeptNo, "d100"))
                                                       .execute();
        printObj(affectedNums);
    }
    
    private static void updateColumn2() {
        int affectedNums = new Update(Employee.class).set(Employee::getFirstName, "wenchang")
                                                     .where(new EQCondition(Employee::getLastName, "Facello"))
                                                     .where(new NEQCondition(Employee::getGender, "F"))
                                                     .where(new EQCondition(Employee::getEmpNo, "10001"))
                                                     .execute();
        printObj(affectedNums);
    }
    
    private static void updateUsingSubQuery() {
        Query subQuery = new Query().from(DeptManager.class)
                                    .distinct(DeptManager::getEmpNo);
        
        int affectedNums = new Update(Employee.class).set(Employee::getFirstName, "wenchang")
                                                     .where(new InCondition(Employee::getEmpNo, subQuery))
                                                     .execute();
        printObj(affectedNums);
    }
    
}
