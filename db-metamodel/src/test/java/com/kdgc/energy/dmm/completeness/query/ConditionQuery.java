package com.kdgc.energy.dmm.completeness.query;

import com.kdgc.energy.dmm.EnvConfig;
import com.kdgc.energy.dmm.completeness.AbstractTest;
import com.kdgc.energy.dmm.completeness.entity.Employee;
import com.kdgc.energy.dmm.condition.*;
import com.kdgc.energy.dmm.query.Query;
import com.kdgc.energy.dmm.time.TimeAnalyser;

import java.util.List;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/18
 */
public class ConditionQuery extends AbstractTest {
    public static void main(String[] args) {
        EnvConfig.setDataSource();
        EnvConfig.onDebug();
//        gtTest();
//        ltTest();
//        eqTest();
//        likeTest();
//        neqTest();
        orTest();
        TimeAnalyser.INST.close();
    }
    
    private static void gtTest() {
        // 带条件的查询 hireDate >= 1990-01-01
        List<Employee> list = new Query().from(Employee.class)
                                         .select()
                                         .where(new EQCondition(Employee::getHireDate, "1990-01-01"))
                                         .execute()
                                         .list();
        printList(list);
    }
    
    private static void ltTest() {
        // 带条件的查询 hireDate >= 1990-01-01
        List<Employee> list = new Query().select()
                                         .from(Employee.class, "t")
                                         .where(new LTCondition(Employee::getHireDate, "1990-01-01"))
                                         .execute()
                                         .list();
        printList(list);
    }
    
    private static void eqTest() {
        List<Employee> list = new Query().select()
                                         .from(Employee.class)
                                         .where(new EQCondition(Employee::getFirstName, "Sumant"))
                                         .execute()
                                         .list();
        printList(list);
    }
    
    private static void likeTest() {
        List<Employee> list = new Query().select()
                                         .from(Employee.class)
                                         .where(new LikeCondition(Employee::getFirstName, "%jan%"))
                                         .execute()
                                         .list();
        printList(list);
    }
    
    private static void neqTest() {
        List<Employee> list = new Query().select()
                                         .from(Employee.class)
                                         .where(new NEQCondition(Employee::getLastName, "Cappelletti"))
                                         .execute()
                                         .list();
        printList(list);
    }
    
    private static void betweenAndTest() {
        List<Employee> list = new Query().select()
                                         .from(Employee.class)
                                         .where(new BetweenCondition(Employee::getBirthDate, "1953-09-02", "1953-09-04"))
                                         .execute()
                                         .list();
        printList(list);
    }
    
    private static void orTest() {
        Condition condition1 = new EQCondition(Employee::getFirstName, "Sumant");
        Condition condition2 = new EQCondition(Employee::getLastName, "Cappelletti");
        Condition orCondition = new OrCondition(condition1, condition2);
        
        List<Employee> list = new Query().select()
                                         .from(Employee.class)
                                         .where(orCondition)
                                         .execute()
                                         .list();
        printList(list);
    }
}
