package com.kdgc.energy.dmm.completeness.query;

import com.kdgc.energy.dmm.EnvConfig;
import com.kdgc.energy.dmm.completeness.AbstractTest;
import com.kdgc.energy.dmm.completeness.entity.Employee;
import com.kdgc.energy.dmm.query.Query;
import com.kdgc.energy.dmm.time.TimeAnalyser;

import java.util.List;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/21
 */
public class DistinctQueryTest extends AbstractTest {
    public static void main(String[] args) {
        EnvConfig.setDataSource();
        EnvConfig.onDebug();
        distinctType1();
        TimeAnalyser.INST.close();
    }
    
    private static void distinctType1() {
        List<String> list = new Query().from(Employee.class)
                                       .select(Employee::getLastName)
                                       .distinct()
                                       .execute()
                                       .list();
        
        printList(list);
    }
    
    private static void distinctType2() {
        List<String> list = new Query().from(Employee.class)
                                       .distinct(Employee::getLastName)
                                       .execute()
                                       .list();
        
        printList(list);
    }
}
