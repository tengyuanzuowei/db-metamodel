package com.kdgc.energy.dmm.completeness.query;

import com.kdgc.energy.dmm.EnvConfig;
import com.kdgc.energy.dmm.completeness.AbstractTest;
import com.kdgc.energy.dmm.completeness.entity.Department;
import com.kdgc.energy.dmm.completeness.entity.DeptEmp;
import com.kdgc.energy.dmm.condition.OnCondition;
import com.kdgc.energy.dmm.query.Query;
import com.kdgc.energy.dmm.time.TimeAnalyser;

import java.util.List;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/21
 */
public class MultiTableQueryTest extends AbstractTest {
    public static void main(String[] args) {
        EnvConfig.setDataSource();
        EnvConfig.onDebug();
        calcDepartmentEmployeeNum();
        TimeAnalyser.INST.close();
    }
    
    /**
     * 计算部门人数
     */
    private static void calcDepartmentEmployeeNum() {
        List<?> list = new Query().from(Department.class, DeptEmp.class)
                                  .select(Department::getDeptName)
                                  .count("num")
                                  .where(new OnCondition(Department::getDeptNo, DeptEmp::getDeptNo))
                                  .groupBy(Department::getDeptName)
                                  .execute()
                                  .list();
        printList(list);
    }
}
