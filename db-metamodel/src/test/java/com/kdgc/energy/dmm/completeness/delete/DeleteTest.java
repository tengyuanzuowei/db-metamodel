package com.kdgc.energy.dmm.completeness.delete;

import com.kdgc.energy.dmm.EnvConfig;
import com.kdgc.energy.dmm.completeness.AbstractTest;
import com.kdgc.energy.dmm.completeness.entity.Department;
import com.kdgc.energy.dmm.completeness.entity.Employee;
import com.kdgc.energy.dmm.condition.BetweenCondition;
import com.kdgc.energy.dmm.condition.EQCondition;
import com.kdgc.energy.dmm.condition.NEQCondition;
import com.kdgc.energy.dmm.delete.Delete;
import com.kdgc.energy.dmm.time.TimeAnalyser;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/19
 */
public class DeleteTest extends AbstractTest {
    public static void main(String[] args) throws Exception {
        EnvConfig.setDataSource();
        EnvConfig.onDebug();
        deleteUsingBetween();
        TimeAnalyser.INST.close();
    }
    
    private static void deleteUsingBetween() {
        int num = new Delete(Employee.class).where(new BetweenCondition(Employee::getBirthDate, "1952-04-19", "1952-04-20"))
                                            .execute();
        printObj(num);
    }
    
    private static void deleteTable() {
        int affectedNum = new Delete(Department.class).execute();
        printObj(affectedNum);
    }
    
    private static void deleteByColumn() {
        int affectedNum = new Delete(Department.class).where(new EQCondition(Department::getDeptName, "Finance"))
                                                      .execute();
        printObj(affectedNum);
    }
    
    private static void deleteByColumns() {
        int affectedNum = new Delete(Department.class).where(new EQCondition(Department::getDeptName, "Marketing"))
                                                      .where(new NEQCondition(Department::getDeptNo, "d100"))
                                                      .execute();
        printObj(affectedNum);
    }
}
