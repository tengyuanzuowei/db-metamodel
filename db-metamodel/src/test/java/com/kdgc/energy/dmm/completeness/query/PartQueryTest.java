package com.kdgc.energy.dmm.completeness.query;

import com.kdgc.energy.dmm.EnvConfig;
import com.kdgc.energy.dmm.completeness.AbstractTest;
import com.kdgc.energy.dmm.completeness.entity.Department;
import com.kdgc.energy.dmm.query.Query;

import java.util.Map;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/18
 */
public class PartQueryTest extends AbstractTest {
    public static void main(String[] args) {
        EnvConfig.setDataSource();
        
        //查询1条数据
        queryOne();
        
        //查询1条数据
//        queryOne2();
    }
    
    /**
     * 查询1条数据
     */
    private static void queryOne() {
        Department department = new Query().from(Department.class)
                                           .select()
                                           .limit(1)
                                           .execute()
                                           .one();
        printObj(department);
    }
    
    /**
     * 查询1条数据
     */
    private static void queryOne2() {
        Map<String, Object> map = new Query().from(Department.class)
                                             .select()
                                             .limit(1)
                                             .execute()
                                             .map();
        printObj(map);
    }
}
