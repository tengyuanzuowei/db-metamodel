package com.kdgc.energy.dmm.completeness.join;

import com.kdgc.energy.dmm.EnvConfig;
import com.kdgc.energy.dmm.completeness.AbstractTest;
import com.kdgc.energy.dmm.completeness.entity.Department;
import com.kdgc.energy.dmm.completeness.entity.DeptEmp;
import com.kdgc.energy.dmm.completeness.entity.Employee;
import com.kdgc.energy.dmm.completeness.entity.Salary;
import com.kdgc.energy.dmm.condition.EQCondition;
import com.kdgc.energy.dmm.join.InnerJoin;
import com.kdgc.energy.dmm.join.LeftJoin;
import com.kdgc.energy.dmm.join.RightJoin;
import com.kdgc.energy.dmm.query.Query;
import com.kdgc.energy.dmm.time.TimeAnalyser;

import java.util.List;
import java.util.Map;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/18
 */
public class JoinQueryTest extends AbstractTest {
    public static void main(String[] args) {
        EnvConfig.setDataSource();
        EnvConfig.onDebug();
//        multiJoin();
        innerJoin();
//        leftJoin();
//        rightJoin();
//        joinOn2Columns();
        TimeAnalyser.INST.close();
    }
    
    
    private static void multiJoin() {
        Map<String, Object> map = new Query().join(new InnerJoin(Employee::getEmpNo, DeptEmp::getEmpNo))
                                             .join(new InnerJoin(DeptEmp::getDeptNo, Department::getDeptNo))
                                             .select(Employee::getFirstName, Employee::getLastName, Department::getDeptName)
                                             .where(new EQCondition(Employee::getEmpNo, "10004"))
                                             .execute()
                                             .one();
        printObj(map);
    }
    
    private static void joinOn2Columns() {
        List<Map<String, Object>> list = new Query().join(new InnerJoin(Employee::getEmpNo, Salary::getEmpNo, Employee::getFirstName, Salary::getEmpNo))
                                                    .select()
                                                    .where(new EQCondition(Employee::getEmpNo, "10004"))
                                                    .execute()
                                                    .list();
        printList(list);
    }
    
    private static void innerJoin() {
        List<Map<String, Object>> list = new Query().join(new InnerJoin(Employee::getEmpNo, Salary::getEmpNo))
                                                    .select()
                                                    .where(new EQCondition(Employee::getEmpNo, "10004"))
                                                    .execute()
                                                    .list();
        printList(list);
    }
    
    private static void leftJoin() {
        List<Map<String, Object>> list = new Query().join(new LeftJoin(Employee::getEmpNo, Salary::getEmpNo))
                                                    .select()
                                                    .where(new EQCondition(Employee::getEmpNo, "10004"))
                                                    .execute()
                                                    .list();
        printList(list);
    }
    
    private static void rightJoin() {
        List<Map<String, Object>> list = new Query().join(new RightJoin(Employee::getEmpNo, Salary::getEmpNo))
                                                    .select()
                                                    .where(new EQCondition(Employee::getEmpNo, "10004"))
                                                    .execute()
                                                    .list();
        printList(list);
    }
}
