package com.kdgc.energy.dmm.completeness.query;

import com.kdgc.energy.dmm.EnvConfig;
import com.kdgc.energy.dmm.completeness.AbstractTest;
import com.kdgc.energy.dmm.completeness.entity.DeptManager;
import com.kdgc.energy.dmm.completeness.entity.Salary;
import com.kdgc.energy.dmm.condition.InCondition;
import com.kdgc.energy.dmm.query.Query;
import com.kdgc.energy.dmm.query.Sort;
import com.kdgc.energy.dmm.time.TimeAnalyser;

import java.util.List;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/18
 */
public class SubQueryTest extends AbstractTest {
    public static void main(String[] args) {
        EnvConfig.setDataSource();
        EnvConfig.onDebug();
        subQuery2();
        TimeAnalyser.INST.close();
    }
    
    private static void subQuery2() {
        Query subQuery = new Query().from(DeptManager.class)
                                    .select(DeptManager::getEmpNo);
        List<Salary> list = new Query().from(Salary.class)
                                       .select()
                                       .where(new InCondition(Salary::getEmpNo, subQuery))
                                       .groupBy(Salary::getEmpNo)
                                       .orderBy(Salary::getSalary, Sort.DESC)
                                       .execute()
                                       .list();
        
        printList(list);
    }
    
    private static void subQuery1() {
        Query subQuery = new Query().from(DeptManager.class)
                                    .select(DeptManager::getEmpNo);
        int count = new Query().from(Salary.class)
                               .count()
                               .where(new InCondition(Salary::getEmpNo, subQuery))
                               .execute()
                               .getInt();
        
        printObj(count);
    }
}
