package com.kdgc.energy.dmm.completeness.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Date;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/17
 */
@Entity(name = "employees")
@Getter
@Setter
@ToString
public class Employee implements Serializable {
    
    private static final long serialVersionUID = 1355281741180685249L;
    @Column
    private Long empNo;
    
    @Column
    private Date birthDate;
    
    @Column
    private String firstName;
    
    @Column
    private String lastName;
    
    @Column
    private Character gender;
    
    @Column
    private Date hireDate;
}
