package com.kdgc.energy.dmm.completeness;

import com.kdgc.energy.dmm.EnvConfig;
import com.kdgc.energy.dmm.time.TimeAnalyser;

import java.util.List;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/18
 */
public abstract class AbstractTest {
    
    public static void printObj(Object obj) {
        System.out.println(obj);
    }
    
    public static void printList(List<?> list) {
        for (Object o : list) {
            System.out.println(o);
        }
    }
    
    public static void onTest() {
        EnvConfig.setDataSource();
        EnvConfig.onDebug();
    }
    
    public static void close() {
        TimeAnalyser.INST.close();
    }
}
