package com.kdgc.energy.dmm.completeness.query;

import com.kdgc.energy.dmm.EnvConfig;
import com.kdgc.energy.dmm.completeness.AbstractTest;
import com.kdgc.energy.dmm.completeness.entity.Employee;
import com.kdgc.energy.dmm.query.Query;
import com.kdgc.energy.dmm.time.TimeAnalyser;

import java.util.Map;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/21
 */
public class CountQueryTest extends AbstractTest {
    public static void main(String[] args) {
        EnvConfig.setDataSource();
        EnvConfig.onDebug();
        count();
        TimeAnalyser.INST.close();
    }
    
    private static void count() {
        int num = new Query().from(Employee.class)
                             .count()
                             .execute()
                             .getInt();
        
        printObj(num);
    }
    
    private static void countWithAlias() {
        int num = new Query().from(Employee.class)
                             .count("num")
                             .execute()
                             .getInt();
        
        printObj(num);
    }
    
    private static void countWithAlias2() {
        Map<?, ?> map = new Query().from(Employee.class)
                                   .count("num")
                                   .execute()
                                   .map();
        
        printObj(map);
    }
}
