package com.kdgc.energy.dmm.completeness.insert;

import com.kdgc.energy.dmm.EnvConfig;
import com.kdgc.energy.dmm.Environment;
import com.kdgc.energy.dmm.completeness.AbstractTest;
import com.kdgc.energy.dmm.completeness.entity.Department;
import com.kdgc.energy.dmm.insert.Insert;
import com.kdgc.energy.dmm.time.TimeAnalyser;

import java.util.concurrent.TimeUnit;

/**
 * @author xu.wenchang
 * @version 1.0 2022/04/19
 */
public class InsertTest extends AbstractTest {
    private static int count = 250;
    
    public static void main(String[] args) throws Exception {
        EnvConfig.setDataSource();
        EnvConfig.onDebug();
        insert2();
//        testStatistics();
        TimeAnalyser.INST.close();
    }
    
    private static void testShowSQL() {
        Environment.showSQL();
        insert();
    }
    
    private static void testStatistics() throws Exception {
        EnvConfig.onDebug();
        
        for (int i = 0; i < 10; i++) {
            insert();
            TimeUnit.SECONDS.sleep(1L);
        }
        
        TimeAnalyser.INST.close();
    }
    
    private static void insert() {
        Department department = new Department();
        department.setDeptNo("d" + (count));
        department.setDeptName("department_" + count++);
        boolean success = new Insert(Department.class).value(department)
                                                      .execute();
        printObj(success);
    }
    
    private static void insert2() {
        boolean success = new Insert(Department.class).value(Department::getDeptNo, "d500")
                                                      .value(Department::getDeptName, "department_500")
                                                      .execute();
        printObj(success);
    }
}
