# 一. 简介

基于[Apache MetaModel](https://metamodel.apache.org/)实现的<b>近似</b>`NoSQL`工具，目的是为了有效减少项目中创建的`DAO`文件以及相关联的`XML`文件，以友好编程的方式来创建常用的`SQL`执行操作，减轻日常的工作量。



# 二. 功能简介及使用

## 1. 设置数据源

```java
DataSource ds = xxx ;
Environment.setDataSource(ds);
```



## 2. 调试 - 显示执行的SQL

执行过程中默认**不**打印`SQL`，需要**手动**开启。

```java
Environment.showSQL();
```

关闭显示执行SQL

```java
Environment.hideSQL();
```



## 3. 执行时间统计与分析

统计每次的执行时间，并且按时段统计分析该时段（每小时）内`SQL`执行的效率，在时段结束后输出平均执行时间较长（`> 3s`）的`SQL`。默认**不**统计分析`SQL`执行时间，需要**手动**开启。



```java
// 开启执行时间统计
Environment.openExecuteTimeStatistics();

// 设置信息输出方式
Environment.setInfoWriter(new xxxInfoWriterImpl());
```



![执行时间统计](https://gitee.com/tengyuanzuowei/db-metamodel/raw/master/db-metamodel/截图/1.png)



## 4. insert

```java
// 方式1 
Department department = new Department();
department.setDeptNo("d500");
department.setDeptName("department_500");
boolean success = new Insert(Department.class).value(department)
  					      .execute();
```



```java
// 方式2
boolean success = new Insert(Department.class).value(Department::getDeptNo, "d500")
                                              .value(Department::getDeptName, "department_500")
                                              .execute();
```



## 5. delete 

```java
// 全表删除
int affectedNum = new Delete(Department.class).execute();
```



```java
// 单字段条件删除
int affectedNum = new Delete(Department.class).where(new EQCondition(Department::getDeptName, 																			    "Finance"))
                                              .execute();
```



```java
// 多字段条件删除
int affectedNum = new Delete(Department.class).where(new EQCondition(Department::getDeptName,                                                                           "Marketing"))
                                              .where(new NEQCondition(Department::getDeptNo,                                                                             "d100"))
                                              .execute();
```



```java
// 范围删除
int num = new Delete(Employee.class).where(new BetweenCondition(Employee::getBirthDate, "1952-04-19", "1952-04-20"))
                                    .execute();
```



## 6. update

```java
// 方式1
int num = new Update(Employee.class).set(Employee::getBirthDate, "1952-09-02")
                                    .where(new BetweenCondition(Employee::getBirthDate, "1953-09-02", "1953-09-04"))
                                    .execute();
```



```java
// 方式2 基于主键更新
Map<String, Object> map = new HashMap<>();
map.put("deptNo", "d100");
map.put("deptName", "test_department3");
int affectedNums = new Update(Department.class).set(map)
                                               .execute();
```



```java
// 方式3 基于主键更新
DeptDTO deptDTO = new DeptDTO();
deptDTO.setDeptNo("d100");
deptDTO.setDeptName("test_department4");
int affectedNums = new Update(Department.class).set(deptDTO)
                                               .execute();
```



```java
// 方式4 基于主键更新
Department department = new Department();
department.setDeptNo("d100");
department.setDeptName("test_department2");
int affectedNums = new Update(Department.class).set(department)
  										    .execute();
```



```java
// 多条件更新
int affectedNums = new Update(Employee.class).set(Employee::getFirstName, "wenchang")
                                             .where(new EQCondition(Employee::getLastName, "Facello"))
                                             .where(new NEQCondition(Employee::getGender, "F"))
                                             .where(new EQCondition(Employee::getEmpNo, "10001"))
                                             .execute();
```



```java
// 子查询更新
Query subQuery = new Query().from(DeptManager.class)
                            .distinct(DeptManager::getEmpNo);
        
int affectedNums = new Update(Employee.class).set(Employee::getFirstName, "wenchang")
                                             .where(new InCondition(Employee::getEmpNo, subQuery))
                                             .execute();
```



## 7. query

### 7.1 条件查询

支持包括：

 - and
- between and
- eq(等于)
- gt(大于)
- gte(大于等于)
- in
- like
- lt(小于)
- lte(小于等于)
- neq(不等于)
- not in
- not like
- on
- or



```java
// 等于条件
List<Employee> list = new Query().from(Employee.class)
                                 .select()
                                 .where(new EQCondition(Employee::getHireDate, "1990-01-01"))
                                 .execute()
                                 .list();
```



```java
// 小于条件
List<Employee> list = new Query().select()
                                 .from(Employee.class, "t")
                                 .where(new LTCondition(Employee::getHireDate, "1990-01-01"))
                                 .execute()
                                 .list();
```



```java
// like条件
List<Employee> list = new Query().select()
                                 .from(Employee.class)
                                 .where(new LikeCondition(Employee::getFirstName, "%jan%"))
                                 .execute()
                                 .list();
```



```java
// between条件
List<Employee> list = new Query().select()
                                 .from(Employee.class)
                                 .where(new BetweenCondition(Employee::getBirthDate, "1953-09-02", "1953-09-04"))
                                 .execute()
                                 .list();
```



```java
// or
Condition condition1 = new EQCondition(Employee::getFirstName, "Sumant");
Condition condition2 = new EQCondition(Employee::getLastName, "Cappelletti");
Condition orCondition = new OrCondition(condition1, condition2);

List<Employee> list = new Query().select()
                                 .from(Employee.class)
                                 .where(orCondition)
                                 .execute()
                                 .list();
```



```java
// 多条件
List<Employee> list = new Query().select()
                                 .from(Employee.class)
 				 .where(new LikeCondition(Employee::getFirstName, "%jan%"))
                                 .where(new BetweenCondition(Employee::getBirthDate, "1953-09-02", "1953-09-04"))
                                 .execute()
                                 .list();
```

### 7.2 分页查询

```java
List<Employee> list = new PageQuery(1, 3).from(Employee.class)
                                         .select()
                                         .where(new LikeCondition(Employee::getFirstName, "%jan%"))
                                         .execute()
                                         .list();
```

### 7.3 函数

支持包括：

- distinct


- count
- max
- min
- sum
- avg
- substring

```java
// 方式1
List<String> list = new Query().from(Employee.class)
                               .select(Employee::getLastName)
                               .distinct()
                               .execute()
                               .list();
```



```java
// 方式2
List<String> list = new Query().from(Employee.class)
                               .distinct(Employee::getLastName)
                               .execute()
                               .list();
```



```java
int num = new Query().from(Salary.class)
                     .count(Salary::getEmpNo)
                     .execute()
                     .getInt();
```



```java
float num = new Query().from(Salary.class)
                       .average(Salary::getSalary, "avg")
                       .execute()
                       .getFloat();
```



```java
Long sum = new Query().from(Salary.class)
                      .sum(Salary::getSalary, "sum")
                      .execute()
                      .getLong();
```



```java
int max = new Query().from(Salary.class)
                     .max(Salary::getSalary)
                     .execute()
                     .getInt();
```



```java
List<String> list = Arrays.asList("Prod", "Huma");
Select select = new Select(Function.SUBSTRING, new Object[]{1, 4}, Department::getDeptName);
Condition condition = new InCondition(select, list);
List<Department> list = new Query().from(Department.class)
                                   .where(condition)
                                   .execute()
                                   .list();
```

### 7.4 排序

```java
// 升序
List<Department> list = new Query().from(Department.class)
                                   .select()
                                   .orderBy(Department::getDeptName)
                                   .execute()
                                   .list();
```



```java
// 降序 
List<Department> list = new Query().from(Department.class)
                                    .select()
                                    .orderBy(Department::getDeptName, Sort.DESC)
                                    .execute()
                                    .list();
```



```java
// 多字段排序
List<Department> list = new Query().from(Department.class)
                                   .select()
                                   .orderBy(Department::getDeptName, Sort.ASC)
                                   .orderBy(Department::getDeptNo, Sort.DESC)
                                   .execute()
                                   .list();
```

### 7.5 分组

```java
List<Map<String, Object>> list = new Query().from(Employee.class)
                                            .select(Employee::getLastName)
                                            .count("num")
                                            .groupBy(Employee::getLastName)
                                            .having(new GTCondition(new Select(Function.COUNT, Employee::getFirstName), 200))
                                            .orderBy(Employee::getLastName)
                                            .execute()
                                            .list();
```



```java
List<LastNameNumDTO> list = new Query().from(Employee.class)
                                       .select(Employee::getLastName)
                                       .count("num")
                                       .groupBy(Employee::getLastName)
                                       .having(new GTCondition(new Select(Function.COUNT, Employee::getFirstName), 200))
                                       .orderBy(Employee::getLastName)
                                       .execute()
                                       .list(LastNameNumDTO.class);
```

### 7.6 join

#### 7.6.1 inner join

```java
List<Map<String, Object>> list = new Query().join(new InnerJoin(Employee::getEmpNo, Salary::getEmpNo))
                                            .select()
                                            .where(new EQCondition(Employee::getEmpNo, "10004"))
                                            .execute()
                                            .list();
```

#### 7.6.2 left join

```java
List<Map<String, Object>> list = new Query().join(new LeftJoin(Employee::getEmpNo, Salary::getEmpNo))
                                            .select()
                                            .where(new EQCondition(Employee::getEmpNo, "10004"))
                                            .execute()
                                            .list();
```

#### 7.6.3 right join

```java
List<Map<String, Object>> list = new Query().join(new RightJoin(Employee::getEmpNo, Salary::getEmpNo))
                                            .select()
                                            .where(new EQCondition(Employee::getEmpNo, "10004"))
                                            .execute()
                                            .list();
```

#### 7.6.4 关联两个字段

```java
List<Map<String, Object>> list = new Query().join(new InnerJoin(Employee::getEmpNo, Salary::getEmpNo, Employee::getFirstName, Salary::getEmpNo))
                                            .select()
                                            .where(new EQCondition(Employee::getEmpNo, "10004"))
                                            .execute()
                                            .list();
```

#### 7.6.5 多表join

```java
Map<String, Object> map = new Query().join(new InnerJoin(Employee::getEmpNo, DeptEmp::getEmpNo))
                                     .join(new InnerJoin(DeptEmp::getDeptNo, Department::getDeptNo))
                                     .select(Employee::getFirstName, Employee::getLastName, Department::getDeptName)
                                     .where(new EQCondition(Employee::getEmpNo, "10004"))
                                     .execute()
                                     .one();
```

### 7.7 多表查询

```java
List<?> list = new Query().from(Department.class, DeptEmp.class)
                          .select(Department::getDeptName)
                          .count("num")
                          .where(new OnCondition(Department::getDeptNo, DeptEmp::getDeptNo))
                          .groupBy(Department::getDeptName)
                          .execute()
                          .list();
```

### 7.8 复杂SQL

对于非常复杂的SQL，建议直接使用SQL语句查询

```java
String sql = "非常非常复杂的sql";

// 上面的sql仅作示例，与下面具体执行逻辑无关，sql中的参数需要自行填充，暂不支持自动填充
List<Employee> list = new SQLQuery(Employee.class, Salary.class).sql(sql())
                                                                .execute()
                                                                .list();      
```

### 7.9 其他

```java
// 只查询第一条数据
Department department = new Query().from(Department.class)
                                   .select()
                                   .limit(1)
                                   .execute()
                                   .one();
```



```java
// 查询第11到20之间的数据
Department department = new Query().from(Department.class)
                                   .select()
                                   .limit(10, 10)
                                   .execute()
                                   .one();
```



## 8. 结果集

支持以下类型结果获取方式：

- list

  自动将行数据填充到`select`实体类对象中，如果`select`包含多表字段或者函数化字段，则填充到`Map`中

- list(Class<?> targetClass) 

  自动将行数据填充到`targetClass`对象中

- map

  自动将行数据填充到`Map`对象中

- one

  自动将第一行数据填充到`select`实体类对象中，如果`select`包含多表字段或者函数化字段，则填充到`Map`中

- one(Class<?> targetClass)

  自动将第一行数据填充到`targetClass`对象中

- getInt

- getLong

- getFloat

- getDouble



# 三. 其他

## 1. 数据源

以`Apache MetaModel`支持的数据源为准，目前只做了关系型数据库方面的逻辑。

![Apache MetaModel支持的数据源](https://gitee.com/tengyuanzuowei/db-metamodel/raw/master/db-metamodel/截图/2.png)

## 2. 事务

事务在业务层处理，本工具不提供事务处理逻辑。



## 3. 实体类规范

目前基于`JPA`规范实现，增删改查逻辑的实体类只需要提供`@Entity`、`@Id`、`@Column`注解即可

```java
// 示例
@Entity(name = "departments")
@Getter
@Setter
@ToString
public class Department implements Serializable {
    private static final long serialVersionUID = -9208600620675404010L;
    @Id
    @Column
    private String deptNo;
    
    @Column
    private String deptName;
}

```

`@Entity`和`@Column`未提供名称信息时，取类名和字段名来替代。转化后的名称全为小写格式，驼峰式的名称用`_`分割连接，例如：`DeptManager -> dept_manager`和`getFromDate -> from_date`



## 4. Bug、需求、建议和想法

请发送邮件至`1029020641@qq.com`，收到后尽快回复。

# 四 最后

最后，感谢大家的使用！